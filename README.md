# CS6500_Final_Project

## This is a project - A Collaborative Filtering-based Movie Recommender System.

### The Repository contains the zip file which has following files and Folders
    cleaned_movies_nd_ratings - contains cleaned files which are used as input for .ipynb file.
    ml-latest-small.zip - contains the raw data (movies.csv and ratings.csv files.)
    Movie_Recommendation_report.pdf
    Movie_Recommendation_report.html
    Movie_recommendation.ipynb
    Pigqueries.txt
    README.md


## Data Cleaning
    The first step is cleaning and converting the data into usable format using PIG Queries.
    We have used the Docker Image("suhothayan/hadoop-spark-pig-hive:2.9.2") for this project which was provided by you to do the PIG assignments.
    ml-latest-small.zip Zip file of Dataset, contains movies.csv and ratings.csv files. By using pig queries (version 2.9.2) in Pigqueries.txt in the above mentioned image, we can convert it to usable files.

## Training and Testing the Model using Jupyter Lab
    We have used the Docker image ("jupyter/all-spark-notebook") for this project which was provided by you to do the Spark and SparkMl assignments.
    Open the Jupyter notebook after starting the namenode.
    We read the files from the folder "cleaned_movies_nd_ratings" to the Movie_recommendation.ipynb file

### After reading the data, perform the line by line execution to test the code.

Note: perform the "pip install findspark" (and necessary packages if needed ) in the terminal of Jupyter Lab to avoid the module errors (restart kernel after installing packages)
